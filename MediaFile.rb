#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('open3')
require_relative('./Config.rb')


module TsMediaPage

    class MediaFile
        attr_accessor :title
        attr_accessor :description
        attr_accessor :categories
        attr_accessor :author
        attr_accessor :album
        attr_accessor :date
        attr_accessor :license
        attr_accessor :lyrics

        def initialize(media_file_name = '')
            @media_file_name = media_file_name
            @config = TsMediaPage::Config.instance()

            #Set the title to the file name so that we have at least
            #something which we can display as title:
            @title = media_file_name
            @categories = []
            @author = nil
            @album = nil
            @license = @config.global_license_text

        end
    end
end
