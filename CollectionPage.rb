#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('erb')

require_relative('./Metadata.rb')


module TsMediaPage

    ##
    # This class handles media collections like playlists and albums.
    class CollectionPage

        attr_accessor :items

        def initialize(
                input_file_name
            )

            output_file_name = File.basename(input_file_name) + '.collection.html'
            super(input_file_name, output_file_name)

            @items = []

            collection_template = IO.read(
                File.expand_path(
                './themes/'
                + @config.theme
                + '/collection.html'
                )
            )
            @collection_erb = ERB.new(collection_template)
        end

        ##
        # Read the content of the collection/playlist.
        def parseContent()
            #TOOD: check the file extension and parse the different formats.
            #The easiest format is a file with the extension .list
            #that just contains file names.
            #A more advanced format is m3u which can also contain metadata.
        end

        def generate()
            @content = @collection_erb.result(binding)
            return super()
        end
    end
end
