#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


module TsMediaPage

    ##
    # This class is responsible for parsing metadata for a media file
    # from a separate .meta file.
    class Metadata

        attr_accessor :title
        attr_accessor :description
        attr_accessor :categories
        attr_accessor :author
        attr_accessor :album
        attr_accessor :date
        attr_accessor :license
        attr_accessor :lyrics

        def initialize(
                media_file_name = ''
            )
            @media_file_name = media_file_name

            @config = TsMediaPage::Config.instance()

            if (!@media_file_name)
                return
            end

            #Set the title to the file name so that we have at least
            #something which we can display as title:
            @title = File.basename(@media_file_name)
            @categories = []
            @author = nil
            @album = nil
            @license = @config.global_license_text

            if (File.exist?(@media_file_name + '.meta'))
                #meta file exists.
                #Read the meta file and extract its metadata:
                @meta_file_content = File.read(@media_file_name + '.meta')

                metadata_start = @meta_file_content.index(/\{\{\@metadata\:/)
                metadata_end = 0
                metadata_block_range = Range.new(0,0)

                if (metadata_start)
                    metadata_end = @meta_file_content.index(/^\}\}$/, metadata_start)

                    metadata_block_range = Range.new(metadata_start, metadata_end)
                    metadata_block = @meta_file_content[metadata_block_range]

                    metadata_block.lines.each do |line|
                        line = line.split('=', 2)
                        key = line[0]
                        value = line[1]

                        case key
                        when 'title'
                            @title = value
                        when 'author'
                            @author = value
                        when 'album'
                            @album = value
                        when 'license'
                            @license = value
                        when 'date'
                            @date = value
                        when 'categories'
                            begin
                                categories = []
                                raw_category_list = value.split(';')
                                raw_category_list.each do |category|
                                    categories << category.strip()
                                end

                                @categories = categories
                            end
                        end
                    end
                end

                #We have read the metadata block with its special syntax and can now
                #strip that block from @meta_file_content and pass the remainder
                #to pandoc to get the description as HTML code:
                if (@meta_file_content)
                    raw_description = ''
                    if (metadata_start)
                        #Strip the metadata block.
                        if (metadata_start >= 1)
                            #There is description text above the metadata block.
                            #Include it in raw_description:
                            raw_description = @meta_file_content[0..(metadata_start-1)]
                        end
                        #Append the description text after the metadata block:
                        raw_description += @meta_file_content[(metadata_end + 2)..@meta_file_content.length()]
                    else
                        #No metadata block present.
                        #All the content of the metadata file is considered
                        #a description text.
                        raw_description = @meta_file_content
                    end

                    Open3.popen2('pandoc -f markdown -t html') {|input, output, pandoc_thread|
                        input.write(raw_description)
                        input.close()
                        @description = output.read()
                    }
                end
            end
        end
    end
end
