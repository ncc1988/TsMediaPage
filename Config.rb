#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'singleton'
require 'gettext'


module TsMediaPage

    ##
    # The version of TsMediaPage
    TSMEDIAPAGE_VERSION = '0.1.0'

    LOCALE_PATH = File.join(File.dirname(__FILE__), 'locale')


    class Config
        include Singleton

        #Include GetText and bind translations:
        include GetText
        bindtextdomain('TsMediaPage', :path => TsMediaPage::LOCALE_PATH)

        attr_accessor :source_path
        attr_accessor :output_path
        attr_accessor :website_title
        attr_accessor :theme
        attr_accessor :layout
        attr_accessor :colour_scheme
        attr_accessor :lang
        attr_accessor :global_license_text
        attr_accessor :thumbnail_size
        attr_accessor :thumbnail_quality
        attr_accessor :audio_profiles
        attr_accessor :video_profiles
        attr_accessor :image_profiles
        attr_accessor :date_string
        attr_accessor :torrents_enabled
        attr_accessor :absolute_url_path

        ##
        # The date when the configuration was last modified:
        attr_accessor :mtime


        def initialize(config_file_name = nil)
            #Load the default configuration.
            #If a configuration file is specified
            #load its settings after the default configuration.
            self.loadDefaultConfig()
            @mtime = Time.at(0)
            if (config_file_name)
                if (self.load(config_file_name))
                    @mtime = File.mtime(config_file_name)
                end
            end
        end

        def addAudioProfile(
                name = nil,
                container = nil,
                file_extension = nil,
                codec = nil,
                samplerate = nil,
                channels = nil,
                bitrate = nil
            )
            @audio_profiles << {
                :name => name,
                :container => container,
                :file_extension => file_extension,
                :codec => codec,
                :samplerate => samplerate,
                :channels => channels,
                :bitrate => bitrate
            }
        end

        def addVideoProfile(
                name = nil,
                container = nil,
                file_extension = nil,
                video_codec = nil,
                video_resolution = nil,
                video_framerate = nil,
                video_bitrate = nil,
                audio_codec = nil,
                audio_samplerate = nil,
                audio_channels = nil,
                audio_bitrate = nil
            )
            @video_profiles << {
                :name => name,
                :container => container,
                :file_extension => file_extension,
                :video_codec => video_codec,
                :video_resolution => video_resolution,
                :video_framerate => video_framerate,
                :video_bitrate => video_bitrate,
                :audio_codec => audio_codec,
                :audio_samplerate => audio_samplerate,
                :audio_channels => audio_channels,
                :audio_bitrate => audio_bitrate
            }
        end

        def addImageProfile(
                name = nil,
                image_format = nil,
                image_resolution = nil,
                image_quality = nil
            )
            @image_profiles << {
                :name => name,
                :image_format => image_format,
                :image_resolution => image_resolution,
                :image_quality => image_quality
            }
        end

        def loadDefaultMediaProfiles()

            self.addAudioProfile(
                'lq',
                'ogg',
                'oga',
                'libvorbis',
                '48k',
                '2',
                '64k'
            )

            self.addAudioProfile(
                'mq',
                'ogg',
                'oga',
                'libvorbis',
                '48k',
                '2',
                '96k'
            )

            self.addAudioProfile(
                'hq',
                'ogg',
                'oga',
                'libvorbis',
                '48k',
                '2',
                '192k'
            )

            self.addVideoProfile(
                'lq', #low quality
                'ogg',
                'ogv',
                'libtheora',
                '352x288',
                '25',
                '256k',
                'libvorbis',
                '48k',
                '2',
                '64k'
            )

            self.addVideoProfile(
                'mq', #medium quality
                'ogg',
                'ogv',
                'libtheora',
                '720x576',
                '25',
                '1500k',
                'libvorbis',
                '48k',
                '2',
                '96k'
            )

            self.addVideoProfile(
                'hq', #high quality
                'ogg',
                'ogv',
                'libtheora',
                '1280x720',
                '25',
                '3000k',
                'libvorbis',
                '48k',
                '2',
                '192k'
            )

            self.addImageProfile(
                'standard',
                'jpeg',
                '1600x1200',
                '0.8'
            )
        end

        def loadDefaultConfig()
            @source_path = Dir.getwd() + '/media'
            @output_path = Dir.getwd() + '/html'
            @website_title = 'TsMediaPage'
            @theme = 'default'
            @layout = 'default'
            @colour_scheme = 'dark'
            @lang = GetText.locale.to_s()
            @global_license_text = 'This media is available under the license ' +
                            '<a href="http://creativecommons.org/licenses/by-sa/4.0">' +
                            'Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)' +
                            '</a>, unless otherwise noted.'
            @thumbnail_size = '320x240'
            @thumbnail_quality = '0.5'
            @audio_profiles = []
            @video_profiles = []
            @image_profiles = []
            @torrents_enabled = false
            @absolute_url_path = ''

            self.loadDefaultMediaProfiles()

            @date_string = '%Y-%m-%d %H:%I %Z'
        end

        def load(file_name = nil)
            if (!file_name)
                self.loadDefaultConfig()
                return false
            end

            if (!File.exist?(file_name))
                puts(
                    sprintf(
                        _('Error while loading the configuration: File "%s" does not exist!'),
                        file_name
                    )
                )
                return false
            end

            file = File.new(file_name, 'r')
            if (!file)
                puts(
                    sprintf(
                        _('Error while loading the configuration: File "%s" cannot be opened!'),
                        file_name
                    )
                )
                return false
            end

            #load each line of the file and check if it contains a
            #configuration option:

            while (line = file.gets())
                if (line.match(/^ *#/))
                    #Lines having a '#' as first non-whitespace character
                    #are a comment and thereby ignored.
                    next
                end

                line = line.split('=', 2)
                key = line[0]
                value = line[1].tr("\n", '')

                case key
                when "source_path"
                    @source_path = value
                when "output_path"
                    @output_path = value
                when "website_title"
                    @website_title = value
                when "theme"
                    @theme = value
                when 'layout'
                    @layout = value
                when 'colour_scheme'
                    @colour_scheme = value
                when 'lang'
                    @lang = value
                when 'global_license_text'
                    @global_license_text = value
                when "thumbnail_size"
                    @thumbnail_size = value
                when "thumbnail_quality"
                    @thumbnail_quality = value
                when 'torrents_enabled'
                    @torrents_enabled = (value == true)
                when 'absolute_url_path'
                    @absolute_url_path = value
                when "audio_profile"
                    begin
                        audio_profile = value.split(';')
                        self.addAudioProfile(
                            audio_profile[0], #name
                            audio_profile[1], #container
                            audio_profile[2], #file_extension
                            audio_profile[3], #codec
                            audio_profile[4], #samplerate
                            audio_profile[5], #channels
                            audio_profile[6]  #bitrate
                        )
                    end
                when "video_profile"
                    begin
                        video_profile = value.split(';')
                        self.addVideoProfile(
                            video_profile[0], #name
                            video_profile[1], #container
                            video_profile[2], #file_extension
                            video_profile[3], #video_codec
                            video_profile[4], #video_resolution
                            video_profile[5], #video_framerate
                            video_profile[6], #video_bitrate
                            video_profile[7], #audio_codec
                            video_profile[8], #audio_samplerate
                            video_profile[9], #audio_channels
                            video_profile[10]  #audio_bitrate
                        )
                    end
                when "image_profile"
                    begin
                        image_profile = value.split(';')
                        self.addImageProfile(
                            image_profile[0], #name
                            image_profile[1], #image_format
                            image_profile[2], #image_resolution
                            image_profile[3]  #image_quality
                        )
                    end
                when "date_string"
                    @date_string = value
                end
            end

            file.close()
            return true
        end

        def save(file_name = nil)
            if (!file_name)
                puts(_('Error while saving the configuration: No file specified!'))
                return false
            end

            file = File.new(file_name, 'w')
            if (!file)
                puts(
                    sprintf(
                        _('Error while saving the configuration: File "%s" cannot be opened!'),
                        file_name
                    )
                )
                return false
            end

            file.puts('source_path=' + @source_path)
            file.puts('output_path=' + @output_path)
            file.puts('website_title=' + @website_title)
            file.puts('theme=' + @theme)
            file.puts('layout=' + @layout)
            file.puts('colour_scheme=' + @colour_scheme)
            file.puts('lang=' + @lang)
            file.puts('global_license_text=' + @global_license_text)
            file.puts('thumbnail_size=' + @thumbnail_size)
            file.puts('date_string=' + @date_string)
            file.puts('torrents_enabled=' + (@torrents_enabled ? '1' : '0'))
            file.puts('absolute_url_path=' + @absolute_url_path)

            @audio_profiles.each { |aprofile|
                file.puts(
                    'audio_profile=' +
                    aprofile[:name] + ';' +
                    aprofile[:container] + ';' +
                    aprofile[:file_extension] + ';' +
                    aprofile[:codec] + ';' +
                    aprofile[:samplerate] + ';' +
                    aprofile[:channels] + ';' +
                    aprofile[:bitrate]
                )
            }
            @video_profiles.each { |vprofile|
                file.puts(
                    'video_profile=' +
                    vprofile[:name] + ';' +
                    vprofile[:container] + ';' +
                    vprofile[:file_extension] + ';' +
                    vprofile[:video_codec] + ';' +
                    vprofile[:video_resolution] + ';' +
                    vprofile[:video_framerate] + ';' +
                    vprofile[:video_bitrate] + ';' +
                    vprofile[:audio_codec] + ';' +
                    vprofile[:audio_samplerate] + ';' +
                    vprofile[:audio_channels] + ';' +
                    vprofile[:audio_bitrate]
                )
            }
            @image_profiles.each { |iprofile|
                file.puts(
                    'image_profile=' +
                    iprofile[:name] + ';' +
                    iprofile[:image_format] + ';' +
                    iprofile[:image_resolution] + ';' +
                    iprofile[:image_quality]
                )
            }

            file.close()
            return true
        end
    end
end
