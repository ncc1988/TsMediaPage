#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('erb')

require_relative('./Config.rb')
require_relative('./Metadata.rb')


module TsMediaPage

    ##
    # This class is the base class for generated HTML pages.
    class Page

        attr_accessor :input_file_name
        attr_accessor :output_file_name

        def initialize(
                input_file_name,
                output_file_name
            )
            if (!input_file_name or !output_file_name)
                return false
            end

            @input_file_name = input_file_name
            @output_file_name = output_file_name

            @config = TsMediaPage::Config.instance()
            @metadata = TsMediaPage::Metadata.new(@input_file_name)

            #Load the base template:
            #(hardcoded for now)
            #TODO: load template file once and use its data
            #in all instances => less disk I/O
            base_template = IO.read(
                File.expand_path('./themes/' +
                                 @config.theme +
                                 '/base.html')
            )
            @base_erb = ERB.new(base_template)

            @content = ''
        end


        def generate()
            return @base_erb.result(binding)
        end
    end
end
