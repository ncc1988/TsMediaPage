#!/usr/bin/env ruby

# This file is part of TsMediaPage
# Copyright (C) 2017-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('erb')

require_relative('./Config.rb')
require_relative('./MediaConverter.rb')
require_relative('./OrgPageGenerator.rb')
require_relative('./Page.rb')
require_relative('./Metadata.rb')


module TsMediaPage

    ##
    # This class represents a media page. It contains methods for creating
    # the HTML pages for media files.
    class MediaPage < Page

        attr_accessor :media_type
        attr_accessor :media_links
        attr_accessor :metadata
        attr_accessor :page_link
        attr_accessor :page_title


        ##
        # -input_file_name- is the unconverted media file,
        # -output_file_name- the converted media file's basename.
        # The name of the corresponding html page is a generic name
        # while the actual converted file names have a suffix consisting
        # of the media profile's name and its defined file extension.
        # -media_type- contains a character for the determined media type.
        # 'i' for an image, 'a' for an audio file, 'v' for a video file.
        def initialize(
                input_file_name,
                output_file_name,
                media_type
            )
            super(input_file_name, output_file_name)

            @media_type = media_type

            @page_link = './' +
                         File.basename(@output_file_name) +
                         '.html'
            @iframe_page_link = './' +
                                File.basename(@output_file_name) +
                                '.iframe.html'
            @torrent_link = './' +
                            File.basename(@output_file_name) +
                            '.torrent'

            @page_title = @metadata.title

            if (media_type.downcase()[0] == 'a')
                @audio_file = true
            end
            if (media_type.downcase()[0] == 'v')
                @video_file = true
            end
            if (media_type.downcase()[0] == 'i')
                @image_file = true
            end

            @media_links = []

            #Load the template for a media's HTML page:
            #(hardcoded for now)
            #TODO: load template file once and use its data
            #in all instances => less disk I/O
            base_template = IO.read(
                File.expand_path('./themes/' +
                                 @config.theme +
                                 '/base.html')
            )
            content_template = IO.read(
                File.expand_path('./themes/' +
                                 @config.theme +
                                 '/media_page.html')
            )
            iframe_template = IO.read(
                File.expand_path('./themes/' +
                                 @config.theme +
                                '/media_page.iframe.html')
            )

            @content_erb = ERB.new(content_template)
            @iframe_erb = ERB.new(iframe_template)

        end


        def generate()
            @content = @content_erb.result(binding)
            return super()
        end


        def generateIframe()
            return @iframe_erb.result(binding)
        end


        def addToCategories()
            org_generator = TsMediaPage::OrgPageGenerator.instance()
            org_generator.addPage(self)
        end


        def generateMediaLinks()
            output_filenames = []
            if (@audio_file)
                @config.audio_profiles.each {|profile|
                    output_filenames << @output_file_name +
                                      '-' + profile[:name] + '.' +
                        profile[:file_extension]
                }
            elsif (@video_file)
                @config.video_profiles.each {|profile|
                    output_filenames << @output_file_name +
                                      '-' + profile[:name] + '.' +
                                      profile[:file_extension]
                }
            elsif (@image_file)
                @config.image_profiles.each {|profile|
                    output_filenames << @output_file_name +
                                      '-' + profile[:name] + '.jpg'
                }
            end

            output_filenames.each {|output_filename|
                output_file_path = @config.output_path + '/' + output_filename
                if (File.exists?(output_file_path))
                    #Get the mime type but set a generic mime typ
                    #as default first:
                    mime_type = 'application/octet-stream'
                    IO.popen(
                        [
                            'file',
                            '--brief',
                            '--dereference',
                            '--mime-type',
                            output_file_path
                        ]
                    ) {|io|
                        mime_type = io.read()
                    }

                    @media_links << {
                        :link => './' + output_filename,
                        :mime_type => mime_type
                    }
                end
            }
        end


        def createPage()
            self.generateMediaLinks()

            #Create the main page first:
            media_page = File.open(
                File.expand_path(@config.output_path) +
                '/' +
                File.basename(@output_file_name) +
                '.html',
                'w'
            )
            media_page.write(self.generate())

            #Then create the iframe page:
            iframe_page = File.open(
                File.expand_path(@config.output_path) +
                '/' +
                File.basename(@output_file_name) +
                '.iframe.html',
                'w'
            )
            iframe_page.write(self.generateIframe())

            #Finally, add this page to all the categories it is a member of:
            self.addToCategories()
        end
    end
end
